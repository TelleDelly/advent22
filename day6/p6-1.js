import {readFileSync} from 'fs'

const input = readFileSync('data.txt').toString()

//Moving window attempt
if(input.length >= 4) {
    const window = [input[0], input[1], input[2], input[3]]
    
    // while loop helpers
    let done = false
    let counter = 4

    //Checking if values are unique
    const checkWindow = () => {
        const check = new Map()
        let valid = true
        window.forEach((pane) => {
            if(!check.has(pane)){
                check.set(pane, 1)
            } else {
                let incre = check.get(pane)
                incre++
                check.set(pane, incre)
            }
        })
        check.forEach((pane) => {
            if(pane > 1) {
                valid = false
            }
        })
        return valid
    }

    // Moving window
    while(!done && counter < input.length){
        if(checkWindow()) {
            done = true
        } else {
            window.shift()
            window.push(input[counter])
            counter++
        }
    }

    console.log(counter)
}