import {readFileSync} from 'fs'

const input = readFileSync('data.txt').toString()

const window = []
const markerLen = 14
//Moving window attempt
if(input.length >= markerLen) {
    for(let i = 0; i < markerLen; i++){
        window.push(input[i])
    }
    
    // while loop helpers
    let done = false
    let counter = markerLen

    //Checking if values are unique
    const checkWindow = () => {
        const check = new Map()
        let valid = true
        window.forEach((pane) => {
            if(!check.has(pane)){
                check.set(pane, 1)
            } else {
                let incre = check.get(pane)
                incre++
                check.set(pane, incre)
            }
        })
        check.forEach((pane) => {
            if(pane > 1) {
                valid = false
            }
        })
        return valid
    }

    // Moving window
    while(!done && counter < input.length){
        if(checkWindow()) {
            done = true
        } else {
            window.shift()
            window.push(input[counter])
            counter++
        }
    }

    console.log(counter)
}