const {readFileSync} = require('fs')
const elfArr = [0,0,0]

const testArr = (val) => {
  if(val > elfArr[0] || val > elfArr[1] || val > elfArr[2]){
    elfArr.push(val)
    elfArr.sort().reverse()
    elfArr.splice(3,1)
  }
}


let input = null

try {
  input = readFileSync('data.txt').toString()
} catch (err) {
  console.error(err)
}

const splitText = input.split(/\n/)
let sum = 0
let max = 0

// console.log(splitText)
for(let i = 0; i<splitText.length; i++){
  if(splitText[i] !== '') {
    sum += parseInt(splitText[i])
  } else {
    testArr(sum, elfArr)
    sum = 0
  }
  // console.log(sum, i, splitText[i])
}

// console.log(elfArr)
console.log(elfArr.reduce((acc, cur) =>  acc+cur, 0))
