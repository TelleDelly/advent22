import { readFileSync } from 'node:fs'

//Elf hands
// A - rock
// B - Paper
// C - Scissors

//My hands
// X - Rock 1
// Y - Paper 2
// Z - Scissors 3

// p2
const points = {
    'X': 1,
    'Y': 2,
    'Z': 3 
}

const winCon = {
    'A': 2,
    'B': 3,
    'C': 1
}

const loseCon = {
    'A': 3,
    'B': 1,
    'C': 2
}

const drawCon = {
    'A': 1,
    'B': 2,
    'C': 3
}


const checkMyPoints = (elfHand, myHand) => {
    let myScore = 0
    if(myHand === 'X'){
        myScore = loseCon[elfHand]
    }
    if(myHand === 'Y') {
        myScore = drawCon[elfHand] + 3
    }
    if(myHand === 'Z') {
        myScore = winCon[elfHand] + 6
    }
    return myScore
}

//File read and transfering into an array
const input = readFileSync('data.txt').toString()
const rounds = input.split(/\n/)

let total = 0

rounds.forEach((round) => {
    const split = round.split(' ')
    total += checkMyPoints(split[0], split[1])
})

console.log(total)
