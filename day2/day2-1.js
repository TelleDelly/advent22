import { readFileSync } from 'node:fs'

//Elf hands
// A - rock
// B - Paper
// C - Scissors

//My hands
// X - Rock
// Y - Paper
// Z - Scissors
const points = {
    'X': 1,
    'Y': 2,
    'Z': 3 
}

const checkMyPoints = (elfHand, myHand) => {
    let myScore = points[myHand] 
    
    if(elfHand === 'A'){
        if(myHand === 'Y'){
            myScore += 6
        }
        if(myHand === 'X') {
            myScore += 3
        }
    }

    if(elfHand === 'B') {
        if(myHand === 'Z') {
            myScore += 6
        }
        if(myHand === 'Y') {
            myScore += 3
        }
    }

    if(elfHand === 'C') {
        if(myHand === 'X') {
            myScore += 6
        }
        if(myHand === 'Z') {
            myScore += 3
        }
    }
    return myScore
}

//File read and transfering into an array
const input = readFileSync('data.txt').toString()
const rounds = input.split(/\n/)

let total = 0

rounds.forEach((round) => {
    const split = round.split(' ')
    total += checkMyPoints(split[0], split[1])
})

console.log(total)
