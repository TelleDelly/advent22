import { readFileSync } from 'fs'

// Reading moves of crane
const input = readFileSync('data.txt').toString().split(/\n/)

// Setting up boxes
// Each stack(array) will be trated as a stack DS
// When moving boxes from a stack a pop operation will be preformed
// When adding boxes to a stack a push operation will be preformed
// Stack head is the end of the array
const s1 = ["B", 'Z', 'T']
const s2 = ['V', 'H', 'T', 'D', 'N']
const s3 = ['B', 'F', 'M', 'D']
const s4 = ['T', 'J', 'G', 'W', 'V', 'Q', 'L']
const s5 = ['W', 'D', 'G', 'P', 'V', 'F', 'Q', 'M']
const s6 = ['V', 'Z', 'Q', 'G', 'H', 'F', 'S']
const s7 = ['Z', 'S', 'N', 'R', 'L', 'T', 'C', 'W']
const s8 = ['Z', 'H', 'W', 'D', 'J', 'N', 'R', 'M']
const s9 = ['M', 'Q', 'L', 'F', 'D', 'S']

const stackRef = {
    1: s1,
    2: s2,
    3: s3,
    4: s4,
    5: s5,
    6: s6,
    7: s7,
    8: s8,
    9: s9,
}

// Each array element of moveList is an array of the moves the cranes needs to preform
// moveList[i][0] is the amount of boxes needed to be moves
// moveList[i][1] is the stack of boxes that will be taken from
// moveList[i][2] is the stack of boxes that the boxes will be placed

const moveList = input.map((element) => {
    const split = element.split(' ')
    split.splice(0,1)
    split.splice(1,1)
    split.splice(2,1)
    split[0] = parseInt(split[0])
    split[1] = parseInt(split[1])
    split[2] = parseInt(split[2])
    return split
})

// move[0] is the number of boxes that need to be moved
// move[1] is the stack of boxes that will be taken from
// move[2] is the stack of boxes that will recieve the boxes

moveList.forEach((move) => {
    const start = stackRef[move[1]].length - move[0]
    const boxes = stackRef[move[1]].splice(start, move[0])
    for(let i = 0; i < boxes.length; i++) {
        stackRef[move[2]].push(boxes[i])
    }
})

const endResult = Object.keys(stackRef).map((stack) => {
    return stackRef[stack].pop()
})

console.log(endResult)
