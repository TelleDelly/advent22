import { readFileSync } from 'fs'

const input = readFileSync('data.txt').toString().split(/\n/)

let count = 0

input.forEach((row,i ) => {
  const sections = row.split(',')
  const elf1 = sections[0].split('-')
  const elf2 = sections[1].split('-')
  

  if((parseInt(elf1[0]) <= parseInt(elf2[0]) && parseInt(elf1[1]) >= parseInt(elf2[1]) )||(parseInt(elf2[0]) <= parseInt(elf1[0]) && parseInt(elf2[1]) >= parseInt(elf1[1]))){
    count ++
  }

 console.log(elf1, elf2)

})

console.log(count)