import { readFileSync } from 'fs'

const input = readFileSync('data.txt').toString().split(/\n/)

let count = 0

input.forEach((row,i ) => {
  const sections = row.split(',')
  const elf1 = sections[0].split('-')
  const elf2 = sections[1].split('-')
  
  const A = parseInt(elf1[0])
  const B = parseInt(elf1[1])
  const C = parseInt(elf2[0])
  const D = parseInt(elf2[1])


  if((A <= D && A >= C) || (B <= D && B >= C) || (C <= B && C >= A) || (D <= B && D >= A)) {
    count++
  }

})

console.log(count)