import { readFileSync } from 'fs'

const input = readFileSync('data.txt').toString().split(/\n/)

const why = {
  'a': 1,
  'b': 2,
  'c': 3,
  'd': 4,
  'e': 5,
  'f': 6,
  'g': 7,
  'h': 8,
  'i': 9,
  'j': 10,
  'k': 11,
  'l': 12,
  'm': 13,
  'n': 14,
  'o': 15,
  'p': 16,
  'q': 17,
  'r': 18,
  's': 19,
  't': 20,
  'u': 21,
  'v': 22,
  'w': 23,
  'x': 24,
  'y': 25,
  'z': 26,
  'A': 27,
  'B': 28,
  'C': 29,
  'D': 30,
  'E': 31,
  'F': 32,
  'G': 33,
  'H': 34,
  'I': 35,
  'J': 36,
  'K': 37,
  'L': 38,
  'M': 39,
  'N': 40,
  'O': 41,
  'P': 42,
  'Q': 43,
  'R': 44,
  'S': 45,
  'T': 46,
  'U': 47,
  'V': 48,
  'W': 49,
  'X': 50,
  'Y': 51,
  'Z': 52,
}

let sum = 0
const groups = []

// input.forEach((element) => {
//   let priority = 0
//   let mid = element.length/2
//   let con1 = element.slice(0,mid)
//   let con2 = element.slice(mid, element.length)

//   for(let i = 0; i < con1.length; i++) {
//     if(con2.includes(con1[i])) {
//       priority = why[con1[i]]
//     }
//   }
//   sum += priority
// })

for(let i = 0; i< input.length; i+=3) {
    groups.push([input[i], input[i+1], input[i+2]])
}

groups.forEach((group) => {
  let priority = 0
  for(let i = 0; i < group[0].length; i++) {
    if(group[1].includes(group[0][i]) && group[2].includes(group[0][i])) {
      priority = why[group[0][i]]
    }
  }
  sum += priority
})
  

console.log(sum)